package by.kolovaitis.currencyscheme.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import by.kolovaitis.currencyscheme.domain.interactors.CurrencyInteractor
import by.kolovaitis.currencyscheme.domain.models.CurrenciesResult
import by.kolovaitis.currencyscheme.domain.models.Currency
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewModel(private val currencyInteractor: CurrencyInteractor) : ViewModel() {
    private var currenciesValue = emptyList<Currency>()

    private val _currencies = MutableLiveData<List<Currency>>().apply {
        value = currenciesValue
    }
    val currency: LiveData<List<Currency>> = _currencies



    private var filter = ""

    private val _isLoading = MutableLiveData<Boolean>().apply {
        value = false
    }
    val isLoading: LiveData<Boolean> = _isLoading

    private val _actualStatus = MutableLiveData<CurrenciesStatus>()
    val actualStatus: LiveData<CurrenciesStatus> = _actualStatus

    fun loadCurrencies() {
        viewModelScope.launch (Dispatchers.IO){
            _isLoading.postValue(true)
            val currenciesResult = currencyInteractor.loadCurrencies()
            currenciesValue = currenciesResult.currencies
            filter(filter)
            setActualStatus(currenciesResult)
            _isLoading.postValue(false)
        }
    }

    private fun setActualStatus(currenciesResult: CurrenciesResult) {
        _actualStatus.postValue(
            when (currenciesResult) {
                CurrenciesResult.Empty -> CurrenciesStatus.NO_DATA
                is CurrenciesResult.FromLocal -> CurrenciesStatus.NOT_ACTUAL
                is CurrenciesResult.FromNetwork -> CurrenciesStatus.ACTUAL
            }
        )
    }

    fun filter(filter:String){
        this.filter = filter
        _currencies.postValue(currenciesValue.filter { currency->
            currency.abbreviation.contains(filter, ignoreCase = true)||
                    currency.name.contains(filter, ignoreCase = true)
        })
    }
}