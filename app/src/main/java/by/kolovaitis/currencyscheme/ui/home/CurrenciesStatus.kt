package by.kolovaitis.currencyscheme.ui.home

import androidx.annotation.StringRes
import by.kolovaitis.currencyscheme.R

enum class CurrenciesStatus(@StringRes val text:Int) {
    ACTUAL(R.string.data_actual), NOT_ACTUAL(R.string.data_not_actual), NO_DATA(R.string.no_data);
}