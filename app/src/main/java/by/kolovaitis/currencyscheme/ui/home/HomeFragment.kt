package by.kolovaitis.currencyscheme.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import by.kolovaitis.currencyscheme.databinding.FragmentHomeBinding
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    private val homeViewModel: HomeViewModel by viewModel()
    private var _binding: FragmentHomeBinding? = null
    private val adapter get() = binding.rvCurrencies.adapter as CurrencyAdapter

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.rvCurrencies.apply {
            adapter = CurrencyAdapter()
            layoutManager = LinearLayoutManager(context)
        }
        setupBindings()

        binding.srUpdate.setOnRefreshListener {
            homeViewModel.loadCurrencies()
        }

        binding.etFilter.doOnTextChanged { text, start, before, count ->
            homeViewModel.filter(text.toString())
        }

        return root
    }

    private fun setupBindings() {
        homeViewModel.currency.observe(viewLifecycleOwner, Observer {
            adapter.refreshDataSet(it)
        })

        homeViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            binding.srUpdate.isRefreshing = it
            binding.rvCurrencies.isVisible = !it
        })
        homeViewModel.actualStatus.observe(viewLifecycleOwner, Observer { status ->
            Snackbar.make(
                binding.root,
                status.text,
                Snackbar.LENGTH_LONG,
            ).show()
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeViewModel.loadCurrencies()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}