package by.kolovaitis.currencyscheme.domain.repositories

import by.kolovaitis.currencyscheme.domain.models.Currency

interface LocalCurrencyRepository {
    suspend fun updateLocalData(currencies: List<Currency>)
    suspend fun getLocalData():List<Currency>?
}