package by.kolovaitis.currencyscheme.domain.interactors

import by.kolovaitis.currencyscheme.domain.models.CurrenciesResult
import by.kolovaitis.currencyscheme.domain.repositories.CurrencyRepository
import by.kolovaitis.currencyscheme.domain.repositories.LocalCurrencyRepository

class CurrencyInteractorImpl(
    private val remoteRepository: CurrencyRepository,
    private val localRepository: LocalCurrencyRepository,
) : CurrencyInteractor {
    override suspend fun loadCurrencies(): CurrenciesResult {
        val remoteCurrencies = remoteRepository.getCurrencies()
        return if (remoteCurrencies.isNullOrEmpty()) {
            val localCurrencies = localRepository.getLocalData()
            if (localCurrencies.isNullOrEmpty()) {
                CurrenciesResult.Empty
            } else {
                CurrenciesResult.FromLocal(localCurrencies)
            }
        } else {
            localRepository.updateLocalData(currencies = remoteCurrencies)
            CurrenciesResult.FromNetwork(remoteCurrencies)
        }
    }
}