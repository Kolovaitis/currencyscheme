package by.kolovaitis.currencyscheme.domain.models

data class Currency(val id:Int, val date:String, val abbreviation:String, val scale:Int, val name:String, val officialRate:Float)