package by.kolovaitis.currencyscheme.domain.models

sealed class CurrenciesResult(val currencies: List<Currency>){
    class FromNetwork(currencies: List<Currency>):CurrenciesResult(currencies)
    class FromLocal(currencies: List<Currency>):CurrenciesResult(currencies)
    object Empty:CurrenciesResult(emptyList())
}
