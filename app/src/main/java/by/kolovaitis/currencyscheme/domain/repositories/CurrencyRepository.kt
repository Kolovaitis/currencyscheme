package by.kolovaitis.currencyscheme.domain.repositories

import by.kolovaitis.currencyscheme.domain.models.Currency

interface CurrencyRepository {
    suspend fun getCurrencies():List<Currency>?
}