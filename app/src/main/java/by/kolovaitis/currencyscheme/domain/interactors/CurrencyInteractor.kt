package by.kolovaitis.currencyscheme.domain.interactors

import by.kolovaitis.currencyscheme.domain.models.CurrenciesResult

interface CurrencyInteractor {
    suspend fun loadCurrencies():CurrenciesResult
}