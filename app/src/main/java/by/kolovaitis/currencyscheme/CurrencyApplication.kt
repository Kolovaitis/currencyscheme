package by.kolovaitis.currencyscheme

import android.app.Application
import by.kolovaitis.currencyscheme.modules.retrofitModule
import by.kolovaitis.currencyscheme.modules.roomModule
import by.kolovaitis.currencyscheme.modules.uiModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CurrencyApplication:Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@CurrencyApplication)
            modules(listOf(retrofitModule, roomModule, uiModule))
        }
    }
}