package by.kolovaitis.currencyscheme.modules

import android.content.Context
import androidx.room.Room
import by.kolovaitis.currencyscheme.data.dao.CurrencyDao
import by.kolovaitis.currencyscheme.data.db.CurrenciesRoomDatabase
import by.kolovaitis.currencyscheme.data.repositories.impl.LocalRepository
import by.kolovaitis.currencyscheme.domain.repositories.LocalCurrencyRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val roomModule = module {
    single<LocalCurrencyRepository> {
        LocalRepository(get())
    }
    single<CurrencyDao> {
            createDatabase(androidContext()).currencyDao()!!
    }

}
private fun createDatabase(context: Context):CurrenciesRoomDatabase{
    return Room.databaseBuilder(
        context,
        CurrenciesRoomDatabase::class.java,
        "database",
    ).build()
}