package by.kolovaitis.currencyscheme.modules


import by.kolovaitis.currencyscheme.data.repositories.impl.RetrofitRepository
import by.kolovaitis.currencyscheme.domain.repositories.CurrencyRepository
import by.kolovaitis.trainee.task11.network.NBRBApi
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val retrofitModule = module {

    single {
        retrofit("https://www.nbrb.by")
    }
    single {
        get<Retrofit>().create(NBRBApi::class.java)
    }
    single<CurrencyRepository>{
        RetrofitRepository(get())
    }
}

private fun retrofit(baseUrl: String) = Retrofit.Builder()
    .baseUrl(baseUrl)
    .addConverterFactory(GsonConverterFactory.create())
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .build()