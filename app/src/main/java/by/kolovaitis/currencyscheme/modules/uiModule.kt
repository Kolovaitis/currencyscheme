package by.kolovaitis.currencyscheme.modules

import by.kolovaitis.currencyscheme.domain.interactors.CurrencyInteractor
import by.kolovaitis.currencyscheme.domain.interactors.CurrencyInteractorImpl
import by.kolovaitis.currencyscheme.ui.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val uiModule = module {
    single<CurrencyInteractor> {
        CurrencyInteractorImpl(get(), get())
    }

    viewModel {
        HomeViewModel(get())
    }
}