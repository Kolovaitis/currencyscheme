package by.kolovaitis.currencyscheme.data.repositories.impl

import by.kolovaitis.currencyscheme.data.models.CurrencyData
import by.kolovaitis.currencyscheme.data.models.toCurrency
import by.kolovaitis.currencyscheme.domain.models.Currency
import by.kolovaitis.currencyscheme.domain.repositories.CurrencyRepository
import by.kolovaitis.trainee.task11.network.NBRBApi

class RetrofitRepository(val api: NBRBApi) : CurrencyRepository {
    override suspend fun getCurrencies(): List<Currency>? {
        return try {
            api
                .getData(periodicity = 0)
                .await()
        } catch (e: Exception) {
            null
        }?.body()?.map(CurrencyData::toCurrency)
    }
}