package by.kolovaitis.currencyscheme.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

import androidx.room.OnConflictStrategy
import by.kolovaitis.currencyscheme.data.models.CurrencyData

@Dao
interface CurrencyDao {
    @Query("SELECT * FROM currencies")
    fun getCurrencies(): List<CurrencyData>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(currency: CurrencyData)
}