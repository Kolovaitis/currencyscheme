package by.kolovaitis.currencyscheme.data.repositories.impl

import by.kolovaitis.currencyscheme.data.dao.CurrencyDao
import by.kolovaitis.currencyscheme.data.models.CurrencyData
import by.kolovaitis.currencyscheme.data.models.fromCurrency
import by.kolovaitis.currencyscheme.data.models.toCurrency
import by.kolovaitis.currencyscheme.domain.models.Currency
import by.kolovaitis.currencyscheme.domain.repositories.LocalCurrencyRepository

class LocalRepository(private val dao: CurrencyDao) : LocalCurrencyRepository {
    override suspend fun updateLocalData(currencies: List<Currency>) {
        currencies.forEach {
            dao.insert(fromCurrency(it))
        }
    }

    override suspend fun getLocalData(): List<Currency>? {
        return dao.getCurrencies()?.map(CurrencyData::toCurrency)
    }
}