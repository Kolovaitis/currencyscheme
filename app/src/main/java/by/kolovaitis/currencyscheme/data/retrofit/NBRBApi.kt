package by.kolovaitis.trainee.task11.network

import by.kolovaitis.currencyscheme.data.models.CurrencyData
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface NBRBApi {
    @GET("/api/exrates/rates?periodicity=0")
    fun getData(
        @Query("periodicity") periodicity: Int?
    ): Deferred<Response<List<CurrencyData>>>
}