package by.kolovaitis.currencyscheme.data.models

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import by.kolovaitis.currencyscheme.domain.models.Currency

@Entity(tableName = "currencies")
data class CurrencyData(
    @PrimaryKey
    @NonNull
    val Cur_ID: Int,
    val Date: String,
    val Cur_Abbreviation: String,
    val Cur_Scale: Int,
    val Cur_Name: String,
    val Cur_OfficialRate: Float
)

fun CurrencyData.toCurrency(): Currency =
    Currency(
        id = this.Cur_ID,
        date = this.Date,
        abbreviation = this.Cur_Abbreviation,
        scale = this.Cur_Scale,
        name = this.Cur_Name,
        officialRate = this.Cur_OfficialRate,
    )

fun fromCurrency(currency: Currency): CurrencyData = with(currency) {
    CurrencyData(
        id,
        date,
        abbreviation,
        scale,
        name,
        officialRate,
    )
}