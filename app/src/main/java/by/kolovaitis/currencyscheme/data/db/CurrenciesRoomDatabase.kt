package by.kolovaitis.currencyscheme.data.db

import androidx.room.RoomDatabase

import androidx.room.Database
import by.kolovaitis.currencyscheme.data.dao.CurrencyDao
import by.kolovaitis.currencyscheme.data.models.CurrencyData


@Database(entities = [CurrencyData::class], version = 1)
abstract class CurrenciesRoomDatabase : RoomDatabase() {
    abstract fun currencyDao(): CurrencyDao?
}